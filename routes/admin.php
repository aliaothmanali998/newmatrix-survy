<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\QuestionsController;
use App\Http\Controllers\Admin\ResponceController;
use App\Http\Controllers\Admin\OptionsController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\SurvyController;
use App\Http\Controllers\Admin\AnswerController;
use App\Http\Controllers\Controller;
use App\Models\Responce;
use App\Notifications\SurvyTaked;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::name('admin.')->group(function () {


    Route::middleware(['auth', 'verified', 'IsAdmin'])->group(function () {

        Route::get('dashboard', [Controller::class, 'dash'])->name('dashboard');
        Route::resource('survies', SurvyController::class);
        Route::resource('answers', AnswerController::class);
        Route::resource('options', OptionsController::class);
        Route::resource('category', CategoryController::class);
        Route::resource('responce', ResponceController::class)->except([
            'create', 'store', 'update', 'edit', 'delete'
        ]);;
        Route::post('checkout/{id}', [ResponceController::class, 'checkout'])->name('responce.checkout');

        Route::get('/', function () {
            Response::find(3)->notify(new SurvyTaked);
            return view('admin.dashboard');
        });


    });


});

