<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Survy extends Model
{
    use HasFactory;
    protected $fillable = [
        'survy', 'description','category_id'
    ];

    public function categories (){ //property
        return $this->belongsTo(Category::class, 'category_id');
    }
    public function questions()
    {
        return $this->hasMany(Question::class);
    }
    public function reseponces ()
    {
        return $this->hasMany(Responce::class);
    }
}
