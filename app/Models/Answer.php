<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    use HasFactory;
    protected $fillable =[
       'q_num', 'answer', 'responce_id'
    ];
    public function responses (){ //property
        return $this->belongsTo(Responce::class, 'responce_id');
    }
}
