<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Question extends Model
{

    use HasFactory;
    protected $fillable =[ 'num', 'text', 'type','survy_id'];

//    public function categories (): BelongsToMany
//    {
//        return $this->belongsToMany(Category::class, 'question_category', 'question_id', 'category_id', 'id', 'id');
//    }



    public function option(){
        return $this->hasMany(Option::class);

    }
//    public function options()
//    {
//        return $this->hasMany(Option::class);
//    }
    public function survy (){ //property
        return $this->belongsTo(Survy::class, 'survy_id');
    }
}
