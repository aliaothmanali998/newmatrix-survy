<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use App\Models\Response;

class Category extends Model
{

    use HasFactory;
    protected $fillable = [
        'name'
    ];



//
//    public function questions (): BelongsToMany
//    {
//        return $this->belongsToMany(Question::class, 'question_category', 'category_id', 'question_id', 'id', 'id');
//    }

    public function survies()
    {
        return $this->hasMany(Survy::class);
    }


public function responces (){
    return $this->hasMany(Responce::class);

}


}
