<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Responce extends Model
{
    use HasFactory;
    protected $fillable = [
        'survy_id', 'category_id', 'statuse'
    ];
//    public function cat(){ //property
//        return $this->belongsTo(Category::class, 'category_id');
//    }
    public function answers(){
        return $this->hasMany(Answer::class);

    }
    public function survies (){
        return $this->belongsTo(Survy::class, 'survy_id');

    }
}



