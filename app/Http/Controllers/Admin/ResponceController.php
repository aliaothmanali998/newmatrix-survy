<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\Answer;
use App\Models\Category;
use App\Models\Responce;
use App\Models\Survy;
use Illuminate\Http\Request;
use App\Notifications\SurvyTaked;

class ResponceController extends Controller
{


    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $categories = Category::get();

        //    dd($categories);
        return view('admin.pages.responce.category', compact(['categories']));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
//        dd($request);
        $input = $request->input();

        $survy_id = $request->id;
        $questions_count = Survy::find($survy_id)->questions()->count();

        $survy = Survy::find($survy_id);


        $responce = Responce::create([
            'survy_id' => $survy_id,
            'category_id' => $survy->category_id,
        ]);


        $survy = Survy::find($request->id);
        $questions = $survy->questions;
        for ($i = 1; $i <= $questions_count; $i++) {
            $c = $i;
            if (is_array($input[$i])) {
                $j = Answer::query()->create([
                    'responce_id' => $responce->id,
                    'answer' => json_encode($input[$i]),
                    'q_num' => $c,
                ]);
            }
            else
                {
                $j = Answer::query()->create([
                    'responce_id' => $responce->id,
                    'answer' => $input[$i],
                    'q_num' => $c,
                ]);
            }


        }
        return redirect('/')->with(['success' => 'your feedback has been recived thanks for your time']);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $category_id = $id;
        $survies = Survy::where('category_id', $id)->get();


        if ($survies)
            return view('admin.pages.responce.survies', compact(['survies']));
        return redirect(route('admin.responce.index'))->with(['mis' => 'there is no responce yet']);


    }



    //dd($answers);


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        Answer::where('responce_id',$id)->delete();
        Responce::where('id', $id)->delete();
        return redirect()->back()->with(['success' => 'the responce has been deleted successfully']);
    }

    public function checkout($id)
    {
        Responce::where('id', $id)->update([
            'statuse' => '1',
        ]);
        return redirect()->back();
    }
}


//store
//
//    dd($request);
//        $category_id= $request->category_id;
//       $respons = Response::create([
//          'name' => $request->name,
//       'email'=> $request->email,
//           'answer'=>$request->answer,
//           'category_id'=>$request->category_id

//       ]);
//        $questions = Question::find(20);
//        $category =$questions->categories;
//        dd($category);

//  Response::create([
//            'answer' => json_encode($data),
//            'category_id' => $category_id,
//        ]);


//      $responce=[$request];
//     dd($responce);
//        $category_id=$request->input('id');
//
//        $category =Category::find($category_id);
//        $questions= $category->questions;


//       $input['category'] = json_encode($data);
//        Response::create([
//            'answer' => json_encode($data),
//            'category_id' => $category_id,


// dd($questions);
//  $responce=
//       dd($responce);
//        Response::where('category_id', $category_id)->get();
//        $responces = $category->responces ;
//        //   dd($responces);
//        foreach ($responces as $responce){
//
//            $answers[$responce->id]=json_decode($responce->answer);
