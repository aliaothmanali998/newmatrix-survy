<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CountHelper;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Question;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class QuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        $questions = Question::get();

        return view('admin.pages.questions.questions', compact(['questions']));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $categories = Category::get();
        return view('admin.pages.questions.add-question', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {


        $validator = Validator::make($request->all(), [


            'text' => 'required',
            'type' => 'required',
            'num' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('admin/question/create')
                ->withErrors($validator)
                ->withInput();
        }


        Question::query()->create(
            [

                'type' => $request->input('type'),
                'text' => $request->input('text'),
                'num' => $request->input('num'),

            ]
        );
        $question = Question::orderby('created_at', 'desc')->first();


        $question->categories()->attach($request->category_id);

        return redirect(route('admin.question.index'))->with(['success' => 'تم إضافة السؤال بنجاح قم بإضافة خيارات له اذا كان يحتاج']);
    }


    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //      return $id;
        $options = Question::where('id', $id)->get();
        //   dd($options);
        return view('admin.pages.questions.test', compact('options'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {

        $question = Question::find($id);
        $categories = Category::get();
        return view('admin.pages.questions.editquestion', compact(['categories', 'question']));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $validator = Validator::make($request->all(), [


            'text' => 'required',
            'type' => 'required',

        ]);

        if ($validator->fails()) {
            return redirect('admin/question/edit')
                ->withErrors($validator)
                ->withInput();
        }
        Question::where('id', $id)->update(
            [

                'type' => $request->input('type'),
                'text' => $request->input('text'),


            ]
        );
        $question = Question::orderby('updated_at', 'desc')->first();


        $question->categories()->sync($request->category_id);

        return redirect(route('admin.question.index'))->with(['success' => 'تم تعديل السؤال بنجاح قم بإضافة خيارات له اذا كان يحتاج']);

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $question = Question::find($id);
        //  dd($question);
        $category_id = $question->categories;
        //   dd($category_id);
        $question->categories()->detach($category_id);


        Question::where('id', $id)->delete();
        return redirect('/admin/question')->with(['success' => 'تم حذف السؤال بنجاح']);
    }
}






//store
//        $request->validate([
//            'name' => ['required', 'string', 'max:255'],
//            'type' => ['required', 'numeric'],
//            'text' => ['required', 'string', 'max:1000'],
//        ]);
//        if ($request->fails()) {
//            return redirect('admin/create')
//                ->withErrors($request)
//                ->withInput();
//        }
//        $data = $request->input('options');
//        dd($data);
//        $options=$request->options;
//
//        dd($options);

//    $str = "Hello world from Java2blog";

# Replace space with underscore

//        $words = explode(' ', $name);
//        foreach ($words as $key => $word){
//            $firstword = $words[0];
//            if($key > 0){
//                $firstword = $firstword.'_' ;
//            }


//        $name = $request->input('name');
//
//        $name = str_replace(" " , "_", $name);

