<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Option;
use App\Models\Question;
use App\Models\Survy;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

class SurvyController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $categories = Category::get();
        return view('admin.pages.survies.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        $survy = Survy::create([
            'survy' => $request->survy,
            'description' => $request->description,
            'category_id' => $request->category_id
        ]);
        $datas = $request->data;
        $survy_id = $survy->id;

        foreach ($datas as $response) {
            $d = [];
            $data = json_decode($response, true);
            foreach ($data as $item) {
                foreach ($item['value'] as $key => $subItem) {


                    $d[$key] = $subItem;


                    //       dd($subItem);


                }
                $newArray = array();

                foreach ($d as $array) {
                    $newArray[$array["key"]] = $array["value"];
                }


                $question = Question::create([
                    'num' => $newArray['num'],
                    'text' => $newArray['text'],
                    'type' => $newArray['type'],
                    'survy_id' => $survy->id,
                ]);

                $question_id = $question->id;
                $opts = $newArray['options'];
                if ($opts) {

                    $options = explode(",-", $opts);
//                    dd($options);
                    foreach ($options as $option) {


                        $op = Option::create([
                            'option' => $option,
                            'question_id' => $question->id,
                        ]);
                    }

//                    }
                }
            }
        }
        return redirect()->back()->with(['success' => "the survy has been created successfully"]);

    }

//dd($datas);


    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        Question::where('survy_id', $id)->delete();
        Survy::where('id', $id)->delete();
        return redirect()->back()->with(['delete'=>'the survy has been deleted successfully']);
    }
}










//                $request->validate([
//                    'name' => [
//                        'required',
//                        Rule::unique('table_name')->where(function ($query) use ($survy_id) {
//                            return $query->where('survy_id', $survy_id);
//                        }),
//                    ],
//                ]);
// إذا وصل الكود إلى هذا النقطة، فإن الفاليداشن نجح
// يمكنك القيام بالعملية التالية هنا، مثل حفظ البيانات في قاعدة البيانات


//       $options=json_decode($request->input('options'));
//      return $options;
//        $options= $request->input('options');
//        $option = explode(',',$options);


