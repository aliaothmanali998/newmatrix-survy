<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Answer;
use App\Models\Survy;
use Illuminate\Http\Request;

class AnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {


    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {

        $survies = Survy::find($id);
        $questions = $survies->questions;
        $responces = $survies->reseponces;


//dd($responces);


        if (isset($responces)) {
            return view('admin.pages.responce.answers', compact(['questions', 'responces']));


        } else {
            return redirect()->back()->with(['mis' => 'there is no responce yet']);
        }


    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}





//        foreach ($questions as $key => $question){
//            Answer::creat([
//                'question' => $key,
//                'answer' => $question,
//                'responce_id'=> $request->id
//            ]);
//    }}
