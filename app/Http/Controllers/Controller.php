<?php

namespace App\Http\Controllers;

use App\Models\Survy;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    public function redirect()
    {
        $usertype = Auth::user()->user_type;

        if ($usertype == '1') {
            return redirect('admin/dashboard');
        } elseif ($usertype == '0') {

            return redirect('/');
        } else {
            redirect()->back();
        }

    }

    public function dash(){
        $survyCount = Survy::get();

        return view ('admin.dashboard',compact('survyCount'));
    }
}
