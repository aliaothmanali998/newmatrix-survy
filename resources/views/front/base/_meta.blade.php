
<head>
    <!-- Basic -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <!-- Site Metas -->
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="shortcut icon" href="{{asset('admin/./assets/img/favicon.png')}}" type="">
    <title>Matrix Web store </title>
    <!-- bootstrap core css -->
    <link rel="stylesheet" type="text/css" href="{{asset('admin/./assets/css/bootstrap.css')}}" />
    <!-- font awesome style -->
    <link href="{{asset('admin/./assets/css/font-awesome.min.css')}}" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="{{asset('admin/./assets/css/style.css')}}" rel="stylesheet" />
    <!-- responsive style -->
    <link href="{{asset('admin/./assets/css/responsive.css')}}" rel="stylesheet" />
    <link id="pagestyle" href="{{asset('admin/./assets/css/material-dashboard.css?v=3.1.0')}}" rel="stylesheet" />
</head>
