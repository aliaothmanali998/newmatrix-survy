<!doctype html>
<html lang="en">

<div></div>

@include('front.base._meta')


<body class="g-sidenav-show  bg-gray-200">

{{--@include('front.side._aside')--}}


<main class="main-content border-radius-lg ">
{{--    <div>--}}
{{--        @include('front.side.navbar')--}}
{{--    </div>--}}

{{--    <div>--}}
{{--        @include('front.side.slider')--}}
{{--    </div>--}}


    <div class="container-fluid py-4">
        <div>
            @yield('content')
        </div>
</main>

{{--<div>--}}
{{--    @include('admin.side.navfix')--}}
{{--</div>--}}

</div>
{{--<div>--}}
{{--    @include('front.side.footer')--}}
{{--</div>--}}



<div>
    @include('front.base._script')

</div>
</body>
</html>
