
@extends('front.layout.app')
@section('content')


    @if(Session::has('success'))
        <div class="alert alert-danger" role="alert">
            <h5> {{Session::get('success')}}</h5>
        </div>
    @endif

    @if(Session::has('quantity'))
        <div class="alert alert-danger" role="alert">
            <h5> {{Session::get('quantity')}} </h5>
        </div>
    @endif

    <!-- product section -->

        <div class="container">

            <div class="heading_container heading_center p-4">
        <p class="border m-4 text-center text-4xl btn-success ">heelp us and take a servy providing us your feedback we will appreciate your ideas</p>

                <div>
                    <h3>
                        select category from  this collection
                    </h3>
                </div>

                <h2 class="m-3 p-3  ">
                    @if(isset($catnav) && $catnav -> count() > 0)
                        @foreach($catnav as $category)

                          <a href="{{route('category.show', $category->id)}}" class="border m-1 btn btn-dribbble pt-3"> <h5>{{$category->name}}</h5></a>
                        @endforeach
                    @endif


                </h2>
            </div>













    <!-- end product section -->




@endsection
