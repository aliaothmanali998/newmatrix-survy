<header class="header_section">
    <div class="container">
        <nav class="navbar navbar-expand-lg custom_nav-container ">
            @if (Route::has('register') || Route::has('login'))
                <a class="font-weight-bolder mb-0 " href="/">Matrix</a>


            @else
{{--                <a class="font-weight-bolder mb-0 " href="{{route('home')}}">Matrix</a>--}}
            @endif

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class=""> </span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">





                <ul class="navbar-nav">
{{--                    <li class="nav-item active">--}}
{{--                        <a class="nav-link" href="{{route('login')}}">login <span class="sr-only">(current)</span></a>--}}
{{--                    </li>  <li class="nav-item active">--}}
{{--                        <a class="nav-link" href="{{route('logout')}}">logout<span class="sr-only">(current)</span></a>--}}
{{--                    </li>--}}
                    @guest
                        <a class="font-weight-bolder mb-0 m-2 " href="{{ route('login') }}">{{ __('Login') }}</a>
                        @if (Route::has('register') || Route::has('login'))
                            <a class="font-weight-bolder mb-0 m-2 " href="{{ route('register') }}">{{ __('Register') }}</a>
                        @endif
                    @else





                        <li class="nav-item dropdown ml-2 mr-2">




                        </li>

                        <ul>

        <span class="m-1 mr-2"> <strong>{{ Auth::user()->name }}</strong></span>


        <a class="font-weight-bolder mb-0 font-weight-light " href="{{route('logout')}}">log out</a>

</ul>




                    @endguest




{{--                    <li class="nav-item active">--}}
{{--                        <a class="nav-link" href="index.html">Home <span class="sr-only">(current)</span></a>--}}
{{--                    </li>--}}
{{--                    <li class="nav-item dropdown">--}}
{{--                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> <span class="nav-label">Pages <span class="caret"></span></a>--}}
{{--                        <ul class="dropdown-menu">--}}
{{--                            <li><a href="about.html">About</a></li>--}}
{{--                            <li><a href="testimonial.html">Testimonial</a></li>--}}
{{--                        </ul>--}}
{{--                    </li>--}}
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link" href="product.html">Products</a>--}}
{{--                    </li>--}}
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link" href="blog_list.html">Blog</a>--}}
{{--                    </li>--}}
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link" href="contact.html">Contact</a>--}}
{{--                    </li>--}}
{{--                ?--}}
                    <form class="form-inline">
                        <button class="btn  my-2 my-sm-0 nav_search-btn" type="submit">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </button>
                    </form>
                </ul>
            </div>
        </nav>
    </div>
</header>
