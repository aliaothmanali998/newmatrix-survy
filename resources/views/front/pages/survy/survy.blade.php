
@extends('front.layout.app-survy')
@section('content')




    @if(Session::has('quantity'))
        <div class="alert alert-danger" role="alert">
            <h5> {{Session::get('quantity')}} </h5>
        </div>
    @endif
    <header class="header_section">
        <div class="container">
            <nav class="navbar navbar-expand-lg custom_nav-container ">
                <h5 class="inline-flex mr-5"> we appreciat your feedback </h5>
                <a href="/" class="font-weight-bolder mb-0 font-weight-light " >Quick survy</a>
            </nav>
        </div>
    </header>
    <!-- product section -->
    <section class="product_section layout_padding">

        <div class="container">




                <form  action="{{ route('responce.store', ['id'=>" $survies->id"]) }}" method="POST" enctype="multipart/form-data">
                @csrf

                @if(isset($survies) && $survies -> count() > 0)
                    @foreach($survies->questions as $survy)
                        @if($survy->type==0)
                                <div class="form-group">
                                    <label for="exampleInputEmail1" class="text-center border cen ">{{$survy->text}}    </label>
                                    <input type="text" name="{{$survy->num}}" class=" fa-text-width" required placeholder="">
                                    <small id="nameHelp" class="form-text text-muted">We'll never share your name with anyone else.</small>
                                </div>
                            @endif
                            @if($survy->type==1)
                                <div id="mydiv1" style="display:none;" class="mt-2 btn-outline-success border alert-danger"><h5 class="alert rounded-r-md text-bg-secondary">please chose one at least*</h5></div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">{{$survy->text}}</label>
                                    <input type="email" name="{{$survy->num}}" class=" fa-text-width"  id=" emailInput" required aria-describedby="emailHelp" placeholder="Enter email">
                                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                                </div>
                            @endif
                            @if($survy->type==2)
                                <div class="form-group">
                                    <label for="formGroupExampleInput">{{$survy->text}}</label>
                                    <textarea type="string" name="{{$survy->num}}" id="answer" required placeholder="your feedback"> enter a falid email</textarea>

                                    <br>
                                </div>
                            @endif
                                    @if($survy->type==3)

                                        <div>
                                            <label for="formGroupExampleInput2">{{$survy->text}}</label>
                                            <select name="{{$survy->num}}" class="    selectpicker" data-live-search="true" required>
                                                @if(isset($survy) && $survy -> count() > 0)

                                                    @foreach($survy->option as $option)


                                                        <option value="{{ $option->option }}">{{ $option->option}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                            @endif
                                    @if($survy->type==4)
                                        <div>
                                            <div>    <label for="formGroupExampleInput2">{{$survy->text}}</label> </div>

                                            <select name="{{$survy->num}}[]" class="selectpicker cen container-fluid border-dark" required multiple="multiple" data-live-search="true" required>
                                                @if(isset($survy) && $survy -> count() > 0)

                                                    @foreach($survy->option as $option)


                                                        <option value="{{ $option->option }}">{{ $option->option}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                            @endif
                                        @if($survy->type==5)

                                <div id="myDiv" style="display:none;" class="mt-2 btn-outline-success border alert-danger"><h5 class="alert rounded-r-md text-bg-secondary">please chose one at least*</h5></div>
                                            <div>    <label for="formGroupExampleInput2">{{$survy->text}}</label> </div>

                                        <div class="form-check checkbox-group">
                                            @foreach($survy->option as $option)

                                            <input class="form-check-input" name="{{$survy->num}}" type="checkbox" value="{{$option->option}}" id="flexCheckDefault" >
                                            <label class="form-check-label" for="flexCheckDefault">
                                                {{$option->option}}
                                            </label>
                                            @endforeach
                                        </div>
                            @endif
                                        @if($survy->type==6)
                                        <div>
                                            <div>    <label for="formGroupExampleInput2">{{$survy->text}}</label> </div>
                                        </div>
                                        <div class="form-check">
                                            @foreach($survy->option as $option)
                                            <input class="form-check-input" type="radio" required name="{{$survy->num}}" value="{{$option->option}}" id="flexRadioDefault1">
                                            <label class="form-check-label" for="$option->id">
                                                {{$option->option}}
                                            </label>
                                            @endforeach
                                        </div>
                            @endif


                    @endforeach
                    @endif


                    <div class="form-group">

                        <div>
                            <button type="submit" class="btn btn-primary p-2 m-2 checks">Submit</button>
                        </div>

                    </div>

                </form>
            </div>









<script>

    function showAndHide() {
        var emailInput = document.getElementById("emailInput");
        var div = document.getElementById("mydiv1");

        if (/\S+@\S+\.\S+/.test(emailInput.value)) { // تحقق مما إذا كانت القيمة المدخلة في الحقل هي عبارة عن بريد إلكتروني
            div.style.display = "block";
            setTimeout(function(){
                div.style.display = "none";
            }, 3000);
        } else {

            div.style.display = "none";

        }
    }



    // $('div.checkbox-group :checkbox:checked').length > 0;
    let x= document.querySelector('form');
     let c = document.querySelectorAll('.checkbox-group input')
  x.addEventListener('submit' , (e) => {

      // console.log(c);
      e.preventDefault();
      c.forEach(cc => {
          if(cc.checked) {
              function showAndHide() {
                  var emailInput = document.getElementById("emailInput");
                  var div = document.getElementById("mydiv1");
                    console.log(emailInput);
                  if (/\S+@\S+\.\S+/.test(emailInput.value)) { // تحقق مما إذا كانت القيمة المدخلة في الحقل هي عبارة عن بريد إلكتروني
                      div.style.display = "block";
                      setTimeout(function(){
                          div.style.display = "none";
                      }, 3000);
                  }
                  else {

                      div.style.display = "none";

                  }
              }

              x.submit();


          }
          elseif(!cc.checked) {
              var div = document.getElementById("myDiv");
              div.style.display = "block";
              setTimeout(function(){
                  div.style.display = "none";
              }, 2000);
          }
      });
  })




</script>



    <!-- end product section -->




@endsection
