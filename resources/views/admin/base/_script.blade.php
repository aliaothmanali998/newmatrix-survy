



<!--   Core JS Files   -->
<script src="{{asset('admin/assets/js/core/popper.min.js')}}" ></script>
<script src="{{asset('admin/assets/js/core/bootstrap.min.js')}}" ></script>
<script src="{{asset('admin/assets/js/plugins/perfect-scrollbar.min.js')}}" ></script>
<script src="{{asset('admin/assets/js/plugins/smooth-scrollbar.min.js')}}" ></script>
<script src="{{asset('admin/assets/js/jquery.min.js')}}" ></script>


<link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.css" />

<script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.js"></script>


<script>
    var win = navigator.platform.indexOf('Win') > -1;
    if (win && document.querySelector('#sidenav-scrollbar')) {
        var options = {
            damping: '0.5'
        }
        Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
    }
</script>

<!-- Github buttons -->
<script async defer src="https://buttons.github.io/buttons.js"></script>
<script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>
<script src="//cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>

<script>
     $(document).ready(function(){
        $('#mytable').DataTable();
     });
</script>
<script>
    $(document).ready(function(){
        $('#myusertable').DataTable();
    });
</script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc --><script src="./assets/js/material-dashboard.min.js?v=3.1.0"></script>
