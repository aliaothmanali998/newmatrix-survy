<!doctype html>
<html lang="en">

<div></div>

    @include('admin.base._meta')


<body class="g-sidenav-show  bg-gray-200">

    @include('admin.side._aside')


    <main class="main-content border-radius-lg ">
<div>
    @include('admin.side.navbar')
</div>




        <div class="container-fluid py-4">
        <div>
            @yield('content')
        </div>
    </main>

        <div>
            @include('admin.side.navfix')
        </div>

    </div>
        <div>
            @include('admin.side.footer')
        </div>



<div>
    @include('admin.base._script')

</div>
</body>
</html>
