@extends('admin.layout.app')
@section('content')


    @if(Session::has('success'))
        <div class="alert alert-success" role="alert">
            <h5>{{Session::get('success')}}</h5>
        </div>
    @endif

    @if(Session::has('unique'))
        <div class="alert alert-success" role="alert">
            <h5>{{Session::get('unique')}}</h5>
        </div>
    @endif




    <div class="container">


       <div class="row ">

            <div class=" col-12 x">
                <br>
                <br>
                <br>
                <form  action="{{ route('admin.survies.store') }}" method="POST" class="to-clone" id="myForm" enctype="multipart/form-data">
                    <input type="hidden" name="_token" id="crsf" value="9eJLZHmJUeDgN7y9N5GxYNuFO5ugEK8oxXwgi0fu">

                    <div id="mydiv" class=" container  ">
                    <div id="hidden-form" class="hidden-form question-form ">

                    <div id="to-clone" class=" mt-6">


{{--                        style="display:none;"--}}
                        <div  class="number" name="num" >
                            <label><h3> num:</h3></label>
                            <input type="id" id="num" name="num" value="1" disabled class=" num input-group-outline" >



                            <script src="script.js"></script>
                        </div>



                    <div class="form-group border m-2">
                        <label for="formGroupExampleInput">type of question select one</label>
                        <select  id="firstInput" onchange="showSecondInput(this.id)" name="type"  class="selict" formnovalidate required>


                            <option value="0"> <h2>  short text</h2> </option>
                            <option value="1"> <h2>  email  </h2> </option>
                            <option value="2"> <h2>  long text</h2> </option>
                            <option value="3"> <h2>  select one it cold be a list or drop down</h2> </option>
                            <option value="4"> <h2>  multi select  </h2> </option>
                            <option value="5"> <h2>  check box</h2> </option>
                            <option value="6"> <h2>  radio button</h2> </option>

                        </select>

                        @error('type')
                        <small class="'form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>





                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">text</label>
                        <textarea  name="text" rows="3"></textarea>
                        @error('text')
                        <small class="'form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>


            <div  class="hiddeninput" name="divhide" style="display:none;">
                <label><h3> رجأً أدخل الخيارات مع وضع فاصلة(-,) بينها</h3></label>
                <input type="text" id="inputField" name="options"  class="hidden-inputField input-group-outline" >

                <ul id="list"></ul>

                <script src="script.js"></script>
            </div></div></div></div>







            </form>

        </div>


   </div>


        <div class="d-inline-block mt-4">
            <button type="button" class="btn btn-info" onclick="showForm()"><h6 class="btn-close-white">اضافة سؤال</h6></button>
            <script src="script.js"></script>
        </div>
        <div>
            <div><h3>please clice on save after every updat and before click on next</h3>
                <button onclick="submitAllForms()" class=" btn-success m-1 "><h4>save</h4></button>
            </div>
            <form action="{{route('admin.survies.store')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div>
                    <input id="data" name="data[]" class="form-controller fa-text-width data" formnovalidate required hidden/>
                </div>
                <div>
                    <label for="formGroupExampleInput2" class="">enter the name of this survyا</label>
                    <div>

                        <div>
                    <input id="survy" name="survy" class=" text-center form-controller fa-text-width data m-2 w-55 " formnovalidate required />
                </div>
                        <div>
                            <label for="formGroupExampleInput2" class="">enter description about this survyا</label>
                            <div>

                                <div>
                                    <input id="description" name="description" class="text-center form-controller fa-text-width data m-2 w-55 " formnovalidate required />
                                </div>

                        <div class="form-group border m-2">
                            <label for="formGroupExampleInput2" class="text-center ">chose the category which this survy belong to. if it has no category please add category first </label>
                            <select name="category_id" class="border-dark-blue bg-gradient-light tab-content" data-live-search="true" required>
                                @if(isset($categories) && $categories -> count() > 0)
                                    @foreach($categories as $category)


                                        <option value="{{ $category->id }}" class="border-end-lg-0 bg-gradient-light table-active form-control-color"><h2>{{ $category->name }}</h2></option>
                                    @endforeach
                                @endif
                            </select>

                            @error('category')
                            <small class="'form-text text-danger">{{$message}}</small>
                            @enderror
                        </div>

                        <div><button type="submit" class="btn btn-primary">send</button></div>

            </form>
        </div>

    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <script  type='text/javascript'>
        function showSecondInput(id){
            console.log(id);
            var firstInput = document.getElementById(id);
            let c=id;
            c++;
            var hiddeninput = document.getElementById(c);
            console.log(hiddeninput);
            if (firstInput.value > 2) {
                hiddeninput.style.display = "block";
                // document.getElementById("secondInput").value = "";
            } else {
                hiddeninput.style.display = "none";
                // document.getElementById("secondInput").value = "";
            }
            console.log(id);
            console.log(c);
        }









        var formInput=[];
        function submitAllForms() {
            var format=[];
            var forms = document.getElementsByClassName('to-clone');

            for (var i = 0; i < forms.length; i++) {
                var pair0 = { key: i, value: i };
                formInput.push(pair0);
                pair0={};
                console.log(formInput);
                var inputs = forms[i] .getElementsByTagName("input");
                for (var t = 0; t < inputs.length; t++) {
                    if(t===0){
                        continue;
                    }
                    console.log(inputs[t].value);
                    var pair1 = { key: inputs[t].name, value: inputs[t].value};
                    formInput.push(pair1);
                    pair1={};
                }
                var sel1 = forms[i] .getElementsByTagName("select");
                for (var t = 0; t < sel1.length; t++) {
                    var pair2 = { key: sel1[t].name, value: sel1[t].value};
                    formInput.push(pair2);
                    pair2={};
                }
                var text = forms[i] .getElementsByTagName("textarea");
                for (var t = 0; t < text.length; t++) {
                    var pair3 = { key: text[t].name, value: text[t].value};
                    formInput.push(pair3);
                    pair3={};
                }
                // var json0 =JSON.stringify(formInput);

                var pair4 = {key: i, value:formInput};
                var json0 =JSON.stringify(formInput);
                format.push(pair4);
                console.log(format);
                // data.append(format);
              var json1 =JSON.stringify(format);
              document.getElementById("data").value=json1;
                formInput=[];
            }



        }




        var clickCount = 0;
        let k =0;

        // دالة تستدعى عند الضغط على الزر
        function showForm() {
            // زيادة قيمة المتغير بواحد

            clickCount++;
            k++;




            // إذا كانت هذه أول نقرة، قم بإظهار الفورم المخفية وإعطائه id
            if (clickCount === 1) {
                var hiddenForm = document.getElementById("hidden-form");
                hiddenForm.style.display = "block";
                hiddenForm.id = "form-" + clickCount;
                var input = hiddenForm.getElementsByTagName("input");
                for (var i = 0; i < input.length; i++) {
                    input[i].id=`${k}`;

                    k++;
                }
                var sel = hiddenForm.getElementsByTagName("select");
                for (var i = 0; i < sel.length; i++) {
                    sel[i].id=`${k}`;
                    k++;
                }
                let divhide= document.querySelector(".hiddeninput");
            if(divhide){
                divhide.id=`${k}`;
                k++;
                console.log(divhide);}

                var text = hiddenForm.getElementsByTagName("textarea");
                for (var i = 0; i < sel.length; i++) {
                   text[i].id=`${k}`;
                    k++;
                }


            }
            else {

                // إذا كانت هذه نقرة ثانية أو أكثر، قم بإنشاء نسخة جديدة من الفورم المخفية وإظهارها وإعطائها id مختلف
                var div = document.querySelector(".to-clone").cloneNode(true);


               div.style.display = "block";
                div.id = "form-" + clickCount;
                let x=document.querySelector('.x');
                var inputs = div.getElementsByTagName("input");
                for (var i = 0; i < inputs.length; i++) {
                        inputs[i].value = "";
                        // inputs[i].name = inputs[i].name+k;
                        inputs[i].id=`${k}`;
                         if(inputs[i].name==='num'){
                             inputs[i].value=`${clickCount}`;
                             inputs[i].disabled=true;
                                                    }
                        k++;
                    }
                    var sel1 = div.getElementsByTagName("select");

                        for (var i = 0; i < sel1.length; i++) {
                            sel1[i].value = "";
                            // inputs[i].name = inputs[i].name+k;
                            sel1[i].id=`${k}`;
                            k++;
                        }
                        let divhid= div.querySelector(".hiddeninput")
                            if(divhid){
                                divhid.id=`${k}`;

                                k++;}

                        var text1 = div.getElementsByTagName("textarea");
                            for (var i = 0; i < text1.length; i++) {
                                text1[i].value = "";
                                // inputs[i].name = inputs[i].name+k;
                                text1[i].id=`${k}`;
                                k++;
                            }







                // إضافة النسخة الجديدة من الفورم إلى صفحة HTML
                x.appendChild(div);



                //
                // var saveButton = document.createElement("button");
                // saveButton.innerHTML = "حفظ";
                // saveButton.style.backgroundColor='green';
                // saveButton.style.marginTop='10px';
                // saveButton.onclick = function() {
                //     // إضافة قيمة الحقل المدخل إلى المصفوفة عند الضغط على زر الحفظ
                //     var inputField = document.getElementById("inputField");
                //     var value = inputField.value;
                //     if (value) {
                //         valuesArray.push(value);
                //         console.log(valuesArray);
                //     }
                // };
                // div.appendChild(saveButton);
                //



                var deleteButton = document.createElement("button");
                deleteButton.innerHTML = "حذف";
                // deleteButton.classList.add("btn-danger ");
                deleteButton.style.backgroundColor='red';
                deleteButton.style.marginTop='10px';
                deleteButton.onclick = function() {
                    // حذف الفورم عند الضغط على زر الحذف
                    div.remove();
                };
                div.appendChild(deleteButton);

                // إضافة الفورم المنسوخ إلى الصفحة

            }
        }
    </script>

@endsection

























{{--        // إضافة زر حذف للفورم المنسوخ--}}
{{--        //                 var selectElement = document.createElement("select");--}}
{{--        //                 selectElement.style.backgroundSize = '10'--}}
{{--        // // تعيين Id للعنصر--}}
{{--        //                 selectElement.id = "mySelect" + clickCount;--}}
{{--        //                 var selectid =selectElement.id;--}}
{{--        //                 var option1 = document.createElement("option");--}}
{{--        //                 option1.value = "0";--}}
{{--        //                 option1.text = "<h6>select</h6>";--}}
{{--        //                 selectElement.appendChild(option1);--}}
{{--        //                 var option2 = document.createElement("option");--}}
{{--        //                 option2.value = "5";--}}
{{--        //                 option2.text = "<h6>select</h6>";--}}
{{--        //                 selectElement.appendChild(option2);--}}
{{--        //                 selectElement.innerHTML = "<h6>اختر نوع السؤال</h6>";--}}
{{--        //                 selectElement.onchange = function() {--}}
{{--        //                     // حذف الفورم عند الضغط على زر الحذف--}}

{{--        // };--}}
{{--        //--}}
{{--        // div.appendChild(selectElement);--}}


    {{--        <script>--}}
    {{--            function sendForm() {--}}
    {{--            document.getElementById('submitForms').addEventListener('click', function() {--}}
    {{--                for (var i = 0; i < k; i++) {--}}
    {{--                    var dynamicName = "formData" + i;--}}

    {{--                dynamicName = new FormData(document.getElementById('"form-" +i'));--}}

    {{--                var formData = new FormData();--}}

    {{--                // Append form data to the main formData object--}}
    {{--                formData.append('dynamicName', "form-" + i);--}}
    {{--                var xhr = new XMLHttpRequest();--}}

    {{--                xhr.open('POST', '/admin/survies', true);--}}

    {{--                // Set headers if required--}}
    {{--                // xhr.setRequestHeader('X-CSRF-TOKEN', document.querySelector('meta[name="csrf-token"]').getAttribute('content'));--}}


    {{--                xhr.onreadystatechange = function () {--}}
    {{--                    if (xhr.readyState === XMLHttpRequest.DONE) {--}}
    {{--                        if (xhr.status === 200) {--}}
    {{--                            console.log(xhr.responseText);--}}
    {{--                        } else {--}}
    {{--                            console.error(xhr.responseText);--}}
    {{--                        }--}}
    {{--                    }--}}
    {{--                };--}}

    {{--                xhr.send(formData);--}}
    {{--            } });}--}}
    {{--//         </script>--}}



{{--        // var butt1 = div.getElementsByTagName("button");--}}
{{--        // for (var i = 0; i < text1.length; i++) {--}}
{{--        //     butt1[i].value = "";--}}
{{--        //     butt1[i].id=`${k}`;--}}
{{--        //     k++;--}}
{{--        // }--}}

{{--        // var butt = hiddenForm.getElementsByTagName("button");--}}
{{--        // for (var i = 0; i < butt.length; i++) {--}}
{{--        //     butt[i].id=`${k}`;--}}
{{--        //     k++;--}}
{{--        // }--}}

{{--        // var options = {--}}
{{--        //     method: 'POST',--}}
{{--        //     headers: {--}}
{{--        //         'Content-Type': 'application/json'--}}
{{--        //     },--}}
{{--        //     body: JSON.stringify(format)--}}
{{--        // };--}}
{{--        // fetch('/admin/survies', options)--}}
{{--        //     .then(response => response.json())--}}
{{--        //     .then(format => console.log(format))--}}
{{--        //     .catch(error => console.error(error));--}}






{{--        //     var formInput=[];--}}
{{--        //     function submitAllForms() {--}}
{{--        //         var format=[];--}}
{{--        //         var forms = document.getElementsByClassName('to-clone');--}}
{{--        //--}}
{{--        //         for (var i = 0; i < forms.length; i++) {--}}
{{--        //             formInput.push(i);--}}
{{--        //             var inputs = forms[i] .getElementsByTagName("input");--}}
{{--        //             for (var t = 0; t < inputs.length; t++) {--}}
{{--        //                   if(t===0){--}}
{{--        //                       continue;--}}
{{--        //                   }--}}
{{--        //                 console.log(inputs[t].value);--}}
{{--        //               formInput.push(inputs[t].value);--}}
{{--        //--}}
{{--        //             }--}}
{{--        //             var sel1 = forms[i] .getElementsByTagName("select");--}}
{{--        //             for (var t = 0; t < sel1.length; t++) {--}}
{{--        //--}}
{{--        //                 formInput.push(sel1[t].value);--}}
{{--        //--}}
{{--        //--}}
{{--        //             }--}}
{{--        //             var text = forms[i] .getElementsByTagName("textarea");--}}
{{--        //             for (var t = 0; t < text.length; t++) {--}}
{{--        //--}}
{{--        //                 formInput.push(text[t].value);--}}
{{--        //--}}
{{--        //--}}
{{--        //             }--}}
{{--        //--}}
{{--        //             format.push(formInput);--}}
{{--        //             console.log(format);--}}
{{--        //             data.value=format;--}}
{{--        //             formInput=[];--}}
{{--        //         }}--}}



{{--    <script>--}}
{{--        let cloneCounter = 0;--}}

{{--        function cloneForm() {--}}
{{--            const originalForm = document.getElementById('originalForm');--}}
{{--            const clonedForm = originalForm.cloneNode(true); // استنساخ العنصر--}}

{{--            cloneCounter++;--}}
{{--            const newId = 'clonedForm' + cloneCounter;--}}

{{--            clonedForm.setAttribute('id', newId); // تعيين id جديد للعنصر المستنسخ--}}

{{--            const deleteButton = document.createElement('button');--}}
{{--            deleteButton.textContent = 'حذف';--}}

{{--            // إضافة حدث click لزر الحذف--}}
{{--            deleteButton.addEventListener('click', function() {--}}
{{--                deleteForm(newId);--}}
{{--            });--}}

{{--            clonedForm.appendChild(deleteButton); // إضافة زر الحذف إلى العنصر المستنسخ--}}

{{--            const clonedFormsContainer = document.getElementById('clonedFormsContainer');--}}
{{--            clonedFormsContainer.appendChild(clonedForm); // إضافة العنصر المستنسخ إلى عنصر الحاوية--}}
{{--        }--}}

{{--        function deleteForm(formId) {--}}
{{--            const formToDelete = document.getElementById(formId);--}}
{{--            formToDelete.remove(); // حذف العنصر--}}
{{--        }--}}
{{--//     </script>--}}




{{--        document.getElementById("addButton").addEventListener("click", function() {--}}
{{--            // قم بإضافة الخيار الجديد هنا--}}




{{--            // تأكد من عدم تنفيذ أي إجراء يؤدي إلى تحويل المستخدم إلى صفحة أخرى--}}
{{--        });--}}


{{--    <script>--}}
{{--        var myArray =[];--}}
{{--        var names={};--}}
{{--        function createArray(id){--}}
{{--            var name ="array_"+id;--}}
{{--            names[name]=[];--}}
{{--        }--}}
{{--        function addItem(id) {--}}

{{--          createArray("id");--}}

{{--            let f = id-5;--}}
{{--            let s = id-4;--}}
{{--            var optionsvalue = document.getElementById(s);--}}
{{--            var inputField = document.getElementById(f);--}}
{{--            var item = inputField.value;--}}

{{--            if (item !== "") {--}}

{{--                var list = document.getElementById("list");--}}
{{--                var listItem = document.createElement("li");--}}
{{--                listItem.innerHTML = item;--}}
{{--                list.appendChild(listItem);--}}
{{--                names.array_id.push(listItem);--}}
{{--                inputField.value = "";--}}
{{--                console.log(listItem);--}}
{{--                console.log(names.array_id);--}}
{{--                optionsvalue.value =  names.array_id;--}}
{{--            }--}}
{{--        }--}}
{{--    </script>--}}



{{--    <script>--}}
{{--        function addQuestion() {--}}
{{--            // إنشاء نسخة جديدة من الفورم--}}
{{--            var div = document.querySelector(".to-clone").cloneNode(true);--}}
{{--            var secondInputContainer = document.getElementById("secondInputContainer");--}}
{{--            if(  secondInputContainer.style.display === "block"){--}}
{{--                secondInputContainer.style.display = "none";--}}
{{--            }--}}

{{--            let x=document.querySelector('.x');--}}
{{--            // إزالة قيم الإدخال من النسخة الجديدة--}}
{{--            var inputs = div.getElementsByTagName("input");--}}
{{--            for (var i = 0; i < inputs.length; i++) {--}}
{{--                inputs[i].value = "";--}}



{{--            }--}}

{{--            // إضافة النسخة الجديدة من الفورم إلى صفحة HTML--}}
{{--         x.appendChild(div);--}}
{{--        }--}}
{{--    </script>--}}





{{--        // $(document).ready(function() {--}}
{{--        //     // Select2 Multiple--}}
{{--        //     $('.select2-multiple').select2({--}}
{{--        //         placeholder: "Select",--}}
{{--        //         allowClear: true--}}
{{--        //     });--}}
{{--        //--}}
{{--        // });--}}
{{--        //--}}
{{--        //--}}
{{--        //     let btn = document.querySelector('#submit-all-forms');--}}
{{--        //     let form = document.querySelectorAll('form');--}}
{{--        //    btn.addEventListner('click' , ()=>{--}}
{{--        //         form.forEach(f=>{--}}
{{--        //             f.submit();--}}
{{--        //         });--}}
{{--        //     });--}}



{{--        // function addOptions() {--}}
{{--        //--}}
{{--        //     var optionsInput = document.getElementById("optionsInput");--}}
{{--        //     var option = optionsInput.value.split(",");--}}
{{--        //--}}
{{--        //--}}
{{--        //     optionsInput.value = "";--}}
{{--        //     console.log(option);--}}
{{--        //     options = [];--}}
{{--        //     options.append(option);--}}
{{--        //     console.log('nice');--}}
{{--        //--}}
{{--        // }--}}
