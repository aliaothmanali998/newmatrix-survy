@extends('admin.layout.app')
@section('content')







    <div class="container">

        <div class="row x">

            <div class=" col-12">
                <br>
                <br>
                <br>
                <form action="{{ route('admin.category.update', $category->id) }}" method="POST">
                    @csrf
                    @method('PATCH')


                    <div class="form-group">
                        <label for="formGroupExampleInput">name of category</label>
                        <input type="text" name="name" id="name-of-category" placeholder="name of category"
                               value="{{$category->name}}">
                        @error('name')
                        <small class="'form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>

                    <br>
                    <div>
                        <button type="submit" class="btn btn-primary p-2 m-2">Submit</button>
                    </div>

            </div>

            </form>
        </div>

        <div class="col-sm">

        </div>
    </div>

    </div>



@endsection
