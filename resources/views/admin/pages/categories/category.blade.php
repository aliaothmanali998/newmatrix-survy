@extends('admin.layout.app')
@section('content')




    @if(Session::has('success'))
        <div class="alert alert-success" role="alert">
            <h5>{{Session::get('success')}}</h5>
        </div>
    @endif







    <div class="container">

        <div class="row x align-content-md-center">

            <div class=" col-12">
                <br>
                <br>
                <br>


                <div class="helen">
                    <a href="{{ Route('admin.category.create') }}" class="btn btn-success p-2  ">create</a>
                </div>

                <table class="table" id="mytable" class="align-content-center btn-outline-dark-blue">
                    <thead>
                    <tr>


                        {{--                        @foreach($responce->answer as $name)--}}

                        <th scope="col">
                            <h2 class=""> Category name</h2>

                        </th>
                        <th scope="col">
                            <h2> operation</h2>

                        </th>


                        {{--                    @endforeach--}}

                    </tr>
                    </thead>

                    <tbody>


                    @foreach($categories as $category)
                        <tr>
                            <td scope="col" class="border">

                                <h3>
                                    {{$category->name}}
                                </h3>
                            </td>

                            <td scope="col" class="border">

                                <div class="helen">

                                    {{--                                    <a href="{{ route('admin.showproduct',$category->id) }}" class="btn btn-primary p-2">show related product</a>--}}
                                    <a href="{{ route('admin.category.edit', $category->id) }}"
                                       class="btn btn-success d-lg-inline-block">edit category name</a>
                                    <form action="{{ route('admin.category.destroy', $category->id) }}" method="POST"
                                          class="d-inline-block">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-primary ">Delete Category</button>

                                    </form>
                                </div>
                            </td>


                        </tr>
                    @endforeach


                    </tbody>


                </table>


            </div>
        </div>
    </div>






@endsection


