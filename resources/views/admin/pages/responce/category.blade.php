@extends('admin.layout.app')
@section('content')






    @if(Session::has('success'))
        <div class="alert alert-success" role="alert">
            <h5>{{Session::get('success')}}</h5>
        </div>
    @endif
    @if(Session::has('mis'))
        <div class="alert alert-success" role="alert">
            <h5>{{Session::get('mis')}}</h5>
        </div>
    @endif







    <div class="container">

        <div class="row x">

            <div class=" col-12">
                <br>


                <table class="table" id="myusertable">
                    <thead>
                    <tr class="border-2">
                        <th scope="col">category name</th>
                        <th scope="col">operation</th>
                    </tr>
                    </thead>
                    <tbody>

                    @if(isset($categories) && $categories -> count() > 0)
                        @foreach($categories as $category)

                            <tr class="border">


                                <th scope="col" class="border-2"><h4>{{ $category->name }}</h4></th>
                                <th scope="col" class="border-2">
                                    <a href="{{route('admin.responce.show', $category->id)}}" class="btn btn-info"><h3>
                                            show responces for {{$category->name}}</h3></a>

                                </th>
                @endforeach
                @endif


            </div>
            <br>


        </div>
    </div>
    </div>






@endsection


