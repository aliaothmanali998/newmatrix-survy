@extends('admin.layout.app')
@section('content')


    @if(Session::has('success'))
        <div class="alert alert-success" role="alert">
            <h5>{{Session::get('success')}}</h5>
        </div>
    @endif
    @if(Session::has('deletemessage'))
        <div class="alert alert-danger" role="alert">
            <h5> {{Session::get('deletemessage')}}</h5>
        </div>
    @endif
    @if(Session::has('successp'))
        <div class="alert alert-success" role="alert">
            <h5>{{Session::get('successp')}}</h5>
        </div>
    @endif

    <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg hallon-main ">








        <div class="container-fluid py-4">


            <div class="helen">

            </div>


            <table class="table"  id="myusertable">
                <thead>
                <tr>
                    <th scope="col">
                        operations
                    </th>

                    @foreach($questions as $question)

                        {{--                        @foreach($responce->answer as $name)--}}



                        <th scope="col">
                            {{$question->text}}
       م                 </th>


                        {{--                    @endforeach--}}
                    @endforeach


                </tr>
                </thead>
                <tbody>

                @foreach($responces as $responce )

                    <tr>
                        <td scope="col" class="border">

                            <form action="{{ route('admin.responce.destroy', $responce->id)}}" method="POST" class="d-inline-block">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-primary ">Delete responce </button>

                            </form>



                                    @if($responce->statuse==0)

                                        <form  action="{{route('admin.responce.checkout', $responce->id)}}" method="POST"  id="myForm" enctype="multipart/form-data">

                                            @csrf
                                            @method('POST')
                                            <button type="submit" class="btn btn-primary ">click if you <br> checked out</button>
                                        </form>

                                    @endif


                        </td>
                        @foreach($responce->answers as  $an)



                            <td scope="col" class="border ">

                                <p> {{$an->answer}} </p>



                                {{--                           {{$answer[$question->name]}}--}}
                            </td>


                        @endforeach

                        {{--            <td scope="col" class="border"> <a href="{{route('admin.responce.destroy', array_search('answer', $answers)  )}}" class="btn btn-danger">delete</a></td>--}}

                    </tr>


@endforeach
                </tbody>


            </table>








@endsection


