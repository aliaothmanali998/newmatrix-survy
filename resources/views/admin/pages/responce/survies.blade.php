@extends('admin.layout.app')
@section('content')


    @if(Session::has('success'))
        <div class="alert alert-success" role="alert">
            <h5>{{Session::get('success')}}</h5>
        </div>
    @endif
    @if(Session::has('deletemessage'))
        <div class="alert alert-danger" role="alert">
            <h5> {{Session::get('deletemessage')}}</h5>
        </div>
    @endif
    @if(Session::has('successp'))
        <div class="alert alert-success" role="alert">
            <h5>{{Session::get('successp')}}</h5>
        </div>
    @endif
    @if(Session::has('mis'))
        <div class="alert alert-success" role="alert">
            <h5>{{Session::get('mis')}}</h5>
        </div>
    @endif
    @if(Session::has('delete'))
        <div class="alert alert-success" role="alert">
            <h5>{{Session::get('delete')}}</h5>
        </div>
    @endif

    <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg hallon-main ">


        <div class="container-fluid py-4 mr-5 ml-5">


            @if(isset($survies) && $survies -> count() > 0)
                @foreach($survies as $survy)
                    <div class="border pt-3 p-lg-2 layout_padding mx-10 my-3 ">
                        <h6 class="d-inline-block">show responces: </h6>   <a
                            href="{{route('admin.answers.show', $survy->id)}}" class="btn btn-success d-inline-block bg-cover">
                            <h5 class="d-inline-block">{{$survy->survy}}</h5></a>
                        @if($survy->reseponces-> count()==0)
                        <form action="{{route('admin.survies.destroy', $survy->id)}}" method="POST" class="ml-5 d-inline-block">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="ml-5 btn btn-primary d-inline-block">Delete Survies</button>
                            </form>
                        @endif
                        <p class="text-center bg-gradient-faded-">
                        <h5 class="text-center color-foreground"> {{$survy->description}}</h5>
                        </p>
                    </div>
                @endforeach
            @endif


        </div>










@endsection


