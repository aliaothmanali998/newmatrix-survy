<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::query()->create(
            [
                'user_type' => '1',
                'name'=>'ali',
                'email'=>'ali@gmail.com',
                'phone'=>'0935001923',
                'address'=>'tartous',
                'password'=>bcrypt('password')]
        );

    }
}
